//
//  ViewController.swift
//  SevenWonders
//
//  Created by Zachary Cole on 2/16/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    let data = NSData(contentsOfURL: NSURL(string: "http://aasquaredapps.com/Class/sevenwonders.json")!)
    
    var json: Array<AnyObject>!
    let tvc = UITableView(frame: UIScreen.mainScreen().bounds, style: UITableViewStyle.Plain)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as? Array
        } catch {
            print(error)
        }
        
//        let d = json[0] as? [String: AnyObject]
        
//        NSLog("%@", (d!["year_built"] as? String!)!)
        

        tvc.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tvc.delegate = self
        tvc.dataSource = self
        tvc.rowHeight = 60
        self.view.addSubview(tvc)
        
        self.tvc.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        
    }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return json.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell:UITableViewCell = tvc.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        var str = self.json[indexPath.row].objectForKey("name") as? String
        str! += ", "
        str! += (self.json[indexPath.row].objectForKey("year_built") as? String)!
        
        cell.textLabel!.numberOfLines = 0
        cell.textLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
        cell.textLabel!.text = str
        
        let url = NSURL(string: self.json[indexPath.row].objectForKey("image_thumb") as! String)
        let data = NSData(contentsOfURL: url!)
        let img = UIImage(data: data!)
        
        cell.imageView!.frame = CGRectMake(0, 0, 50, 50)
        

        cell.imageView!.layer.masksToBounds = false
        cell.imageView!.layer.cornerRadius = 5;
        cell.imageView!.clipsToBounds = true
        
        cell.imageView!.image = img
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

